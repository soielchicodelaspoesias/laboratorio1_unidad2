#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgAdvertencia():
        def __init__(self):
            self.builder = Gtk.Builder()
            self.builder.add_from_file("./ui/ejemplo.ui")

            self.advertencia = self.builder.get_object("dlgAdvertencia")
            self.advertencia.resize(600, 400)

            #boton aceptar
            self.boton_aceptar_advertencia = self.builder.get_object("boton_aceptar_advertencia")
            self.boton_aceptar_advertencia.connect("clicked", self.click_aceptar)

            self.advertencia.show_all()


        def click_aceptar(self, btn=None):
            self.advertencia.destroy()
