#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class Message_Add():
        def __init__(self):
            self.builder = Gtk.Builder()
            self.builder.add_from_file("./ui/ejemplo.ui")


            self.message_add = self.builder.get_object("message_add")
            self.message_add.resize(600, 400)

            # boton aceptar
            self.boton_aceptar = self.builder.get_object("boton_aceptar")
            self.boton_aceptar.connect("clicked", self.click_aceptar)

            self.message_add.show_all()


        def click_aceptar(self, btn=None):
            self.message_add.destroy()
