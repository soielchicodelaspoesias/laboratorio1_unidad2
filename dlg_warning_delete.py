#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk


class dlgWarningDelete():
        def __init__(self):
            self.builder = Gtk.Builder()
            self.builder.add_from_file("./ui/ejemplo.ui")

            self.warning_delete = self.builder.get_object("warning_delete")
            self.warning_delete.resize(600, 400)

            #boton_aceptar
            self.boton_aceptar = self.builder.get_object("aceptar_delete")
            self.boton_aceptar.connect("clicked", self.confirmar_delete)

            #boton cancelar
            self.boton_cancelar = self.builder.get_object("cancelar_delete")
            self.boton_cancelar.connect("clicked", self.cancelar_delete)

            self.warning_delete.show_all()

        def confirmar_delete(self, btn=None):
            self.warning_delete.destroy()


        def cancelar_delete(self, btn=None):
            self.warning_delete.destroy()
